package routing;

import java.util.HashMap;

public class Node {
	
	public int id;
	public int num_of_edges;
	public HashMap<Integer,Integer> conn_weights;
	public boolean visited;
	public boolean isSource;
	public boolean isSink;
	public int curr_cost;
	public int backpointer;
	
	public Node(int i){
		this.id = i;
		this.num_of_edges = 0;
		this.visited = false;
		this.conn_weights = new HashMap<Integer, Integer>();
		this.isSink = false;
		this.isSource = false;
		this.curr_cost = 0;
	}
	
	public void connectTo(Node a, int weight){
		this.num_of_edges++;
		this.conn_weights.put(a.id, weight);
		a.num_of_edges++;
		a.conn_weights.put(this.id, weight);
	}
	
}
