package routing;

import java.util.ArrayList;
import java.util.TreeMap;

public class Graph {
	public ArrayList<Node> nodes;
	public Node source;
	public Node sink;
	
	
	/**
	 * global variables for cycles for each operation
	 */
	private static final int  COST_EVAL = 2; //# of cycles to evaluate cost of routing to node
	private static final int COST_PUT_PQ = 1; //# of cycles to push a value into the PQ
	private static final int COST_POP_PQ = 1; //# of cycles to pop a value from the PQ
	
	private static final int NUM_PARR_WAVE = 4; //# of parallel wavefront expansions at each node
	
	public Graph(){
		this.nodes = new ArrayList<Node>();
	}
	
	public void add(Node node){
		this.nodes.add(node);
	}
	
	public int size(){
		return this.nodes.size();
	}
	
	public ArrayList<Node> allNodes(){
		return this.nodes;
	}
	
	public static void main(String args[]){
		Graph graph = new Graph();
		//constructTestGraph(graph);
		moreRealisticGraph(graph);
		int cycles_seq = graph.rout_seq();
		
		System.out.println("Sequential Routing cycle count = "+cycles_seq);
		
		int cycles_parr = graph.rout_parr();
		
		System.out.println("Parallel Routing cycle count = "+cycles_parr);
	}
	
	public int rout_seq(){
		int cycles = 0;
		
		Node curr = this.source;
		curr.visited = true;
		curr.backpointer = -1; //no backpointer from source
		
		TreeMap<Integer,ArrayList<Integer>> PQ = new TreeMap<Integer,ArrayList<Integer>>();
		
		boolean finish = false;
		int count = 0;
		while (!curr.isSink || finish){
			for (Integer i:curr.conn_weights.keySet()){
				if (curr.backpointer != i){
					cycles += COST_EVAL;
					Node succ = this.getNodeByID(i);
					if (succ.visited){
						int new_cost = curr.curr_cost + curr.conn_weights.get(i);
						if (succ.curr_cost > new_cost){
							succ.backpointer = curr.id;
							succ.curr_cost = new_cost;
							if (PQ.containsKey(succ.curr_cost)){
								ArrayList<Integer> subqueue = PQ.get(succ.curr_cost);
								subqueue.add(i);
								cycles += COST_PUT_PQ; //one cycle to put result into PQ
							}else {
								ArrayList<Integer> subqueue = new ArrayList<Integer>();
								subqueue.add(i);
								PQ.put(succ.curr_cost, subqueue);
								cycles += COST_PUT_PQ; //one cycle to put result into PQ
							}
						}
					}else {
						succ.backpointer = curr.id;
						succ.curr_cost = curr.curr_cost + curr.conn_weights.get(i);
						if (PQ.containsKey(succ.curr_cost)){
							ArrayList<Integer> subqueue = PQ.get(succ.curr_cost);
							subqueue.add(i);
							cycles += COST_PUT_PQ; //one cycle to put result into PQ
						}else {
							ArrayList<Integer> subqueue = new ArrayList<Integer>();
							subqueue.add(i);
							PQ.put(succ.curr_cost, subqueue);
							cycles += COST_PUT_PQ; //one cycle to put result into PQ
						}
						succ.visited = true;
					}
					
				}
			}
			int n = computePQSize(PQ);
			cycles += Math.round(n*Math.log(n)/Math.log(2)); //O(nlog n) cycles = heap sort complexity (base 2)
			//cycles += Math.round(n*Math.log(n)); //O(nlog n) cycles = heap sort complexity (base 10)
			curr = popPQ(PQ, this);
			cycles += COST_POP_PQ; //one cycle to pop result from PQ
			if (curr == null)
				finish = true; //if PQ is empty - expanded everything, didn't find sink.
			count++;
			System.out.println("While loop count = "+count+", cycles after = "+cycles);
		}
		
		return cycles;
	}
	
	private static Node popPQ(TreeMap<Integer,ArrayList<Integer>> PQ, Graph graph){
		
		if (PQ.size() == 0)
			return null;
		else {
			Integer i = PQ.firstKey();
			if (PQ.get(i).size() > 1){
				//int rand = (int) Math.round(Math.random()*(PQ.get(i).size()-1));
				int rand = 0;
				int id = PQ.get(i).get(rand);
				PQ.get(i).remove(rand);
				return graph.getNodeByID(id);
			}else {
				int id = PQ.get(i).get(0);
				PQ.get(i).remove(0);
				PQ.remove(i);
				return graph.getNodeByID(id);
			}
		}
	}
	
	private static int computePQSize(TreeMap<Integer,ArrayList<Integer>> PQ){
		int result = 0;
		
		for (Integer i:PQ.keySet()){
			result += PQ.get(i).size();
		}
		
		return result;
	}
	
	public Node getNodeByID(int i) {
		Node node = null;
		
		for (Node n:this.nodes){
			if (n.id == i)
				node = n;
		}
		
		assert node != null;
		return node;
	}
	
	public void printNodeConnections(){
		for (Node n:this.allNodes()){
			for (Integer i:n.conn_weights.keySet()){
				System.out.println(n.id+" --> "+i+" ["+n.conn_weights.get(i)+"]");
			}
		}
	}
	
	public int rout_parr(){
		int currentTime = 0;
		
		Node curr = this.source;
		curr.visited = true;
		curr.backpointer = -1; //no backpointer from source
		
		PriorityQueue PQ = new PriorityQueue(this);
		ExpandBlock EB = new ExpandBlock(this);
		
		PQ.setEB(EB);
		EB.setPQ(PQ);
		
		PQ.initWriteInput(0, curr);
		
		while (EB.isBusy(currentTime) || PQ.isBusy(currentTime)){
			PQ.processInputsFromEB(currentTime);
			PQ.processInputs(currentTime);
			EB.processInputsFromPQ(currentTime);
			EB.processInputs(currentTime);
			currentTime++;
			System.out.println("Cycle count = "+currentTime);
		}
		
		return currentTime;
	}
	
	private static void constructTestGraph(Graph graph){
		Node one = new Node(1);
		Node two = new Node(2);
		Node three = new Node(3);
		Node four = new Node(4);
		Node five = new Node(5);
		Node six = new Node(6);
		Node seven = new Node(7);
		Node eight = new Node(8);
		Node nine = new Node(9);
		Node ten = new Node(10);
		Node eleven = new Node(11);
		Node twelve = new Node(12);
		Node thirteen = new Node(13);
		Node fourteen = new Node(14);
		
		one.connectTo(two, rand());
		one.connectTo(three, rand());
		one.connectTo(four, rand());
		one.connectTo(five, rand());
		one.isSource = true;
		graph.source = one;
		
		two.connectTo(six, rand());
		two.connectTo(seven, rand());
		two.connectTo(eight, rand());
		
		three.connectTo(nine, rand());
		three.connectTo(ten, rand());
		three.connectTo(eleven, rand());
		
		four.connectTo(twelve, rand());
		four.connectTo(thirteen, rand());
		four.connectTo(fourteen, rand());
		
		//fourteen.isSink = true;
		fourteen.isSink = true;
		graph.sink = fourteen;
		
		graph.add(one);
		graph.add(two);
		graph.add(three);
		graph.add(four);
		graph.add(five);
		graph.add(six);
		graph.add(seven);
		graph.add(eight);
		graph.add(nine);
		graph.add(ten);
		graph.add(eleven);
		graph.add(twelve);
		graph.add(thirteen);
		graph.add(fourteen);
	}
	
	private static void moreRealisticGraph(Graph graph){
		
		Node a = new Node(1);
		Node b = new Node(2);
		Node c = new Node(3);
		Node d = new Node(4);
		Node e = new Node(5);
		Node f = new Node(6);
		Node g = new Node(7);
		Node h = new Node(8);
		Node i = new Node(9);
		Node j = new Node(10);
		Node k = new Node(11);
		Node l = new Node(12);
		Node m = new Node(13);
		Node n = new Node(14);
		Node o = new Node(15);
		Node p = new Node(16);
		Node q = new Node(17);
		
		a.connectTo(b, rand());
		a.connectTo(c, rand());
		a.connectTo(d, rand());
		a.connectTo(e, rand());
		b.connectTo(a, rand());
		b.connectTo(f, rand());
		b.connectTo(g, rand());
		b.connectTo(h, rand());
		c.connectTo(a, rand());
		c.connectTo(i, rand());
		c.connectTo(j, rand());
		c.connectTo(k, rand());
		d.connectTo(a, rand());
		d.connectTo(l, rand());
		d.connectTo(m, rand());
		d.connectTo(n, rand());
		e.connectTo(a, rand());
		e.connectTo(o, rand());
		e.connectTo(p, rand());
		e.connectTo(q, rand());
		
		
		a.isSource = true;
		m.isSink = true;
		graph.source = a;
		
		graph.add(a);
		graph.add(b);
		graph.add(c);
		graph.add(d);
		graph.add(e);
		graph.add(f);
		graph.add(g);
		graph.add(h);
		graph.add(i);
		graph.add(j);
		graph.add(k);
		graph.add(l);
		graph.add(m);
		graph.add(n);
		graph.add(o);
		graph.add(p);
		graph.add(q);
	}
	
	/**
	 * generates random number between 1 and 3, inclusive
	 * @return
	 */
	private static int rand(){
		int rand = (int) Math.round(Math.random()+0.5);
		return rand;
	}
}
