package routing;

import java.util.ArrayList;
import java.util.TreeMap;

public class ExpandBlock {

	//private static final int  COST_EVAL = 1; //# of cycles to evaluate cost of routing to node
	//private static final int COST_POP_PQ = 1; //# of cycles to pop a value from the PQ
	
	private TreeMap<Integer,ArrayList<Integer>> heap = null;
	
	//number of inputs that should be evaluated in each cycle.. i.e. number of input ports to PQ block
	private static final int numOfPorts = 4;
	
	private static final int COST_EB_PER_NODE = 2; //# of cycles to find nodes to expand to
	private static final int COST_SEND_OUTPUT = 2; //# of cycles to send new values to PQ
	
	private ArrayList<Node> tempInputBuffer = null;
	private ArrayList<Node> inputs = null; //currently assume inputs buffer to have infinite depth
	private ArrayList<Node>[] outputs = null;
	private ArrayList<Node>[] tempOutputs = null;
	private Graph graph = null;
	private PriorityQueue PQ = null;
	private ArrayList<Boolean> slotsAvailable = null;
	private ArrayList<Boolean>[] slotsAvailableEBO = null;
	private ArrayList<Boolean> timeSlots = null;
	
	private boolean foundRoute;
	
	public ExpandBlock(Graph g) {
		super();
		inputs = new ArrayList<Node>();
		outputs = new ArrayList[numOfPorts];
		tempOutputs = new ArrayList[numOfPorts];
		slotsAvailable = new ArrayList<Boolean>();
		slotsAvailable.add(true); // not busy at cycle 0
		timeSlots = new ArrayList<Boolean>();
		//timeSlots.add(false);
		tempInputBuffer = new ArrayList<Node>();
		slotsAvailableEBO = new ArrayList[numOfPorts];
		for (int i = 0; i < numOfPorts; i++){
			outputs[i] = new ArrayList<Node>();
			tempOutputs[i] = new ArrayList<Node>();
			slotsAvailableEBO[i] = new ArrayList<Boolean>();
		}
		graph = g;
		heap = new TreeMap<Integer,ArrayList<Integer>>();
		foundRoute = false;
	}
	
	public void setPQ(PriorityQueue pq){
		this.PQ = pq;
	}
	
	public void sendOutputs(ArrayList<Node>[] exInputs, ArrayList<Boolean>[] timeSlots, int currentTime){
		assert exInputs.length == numOfPorts;
		
		for (int i=0; i < numOfPorts; i++){
			if (!this.outputs[i].isEmpty()){
				reserveSlotForSend(timeSlots[i], currentTime);
				Node sendNode = this.outputs[i].remove(0);//remove first Node
				exInputs[i].add(sendNode);
			}else{
				timeSlots[i].add(false);
			}
		}	
	}
	
	public void reserveSlotForSend(ArrayList<Boolean> timeSlots, int time){
		int hops = 0;
		boolean search = hops < COST_SEND_OUTPUT || timeSlots.get(time+hops);
		while (search){
			if (timeSlots.size() == (time+hops)){
				if (hops >= COST_SEND_OUTPUT)
					timeSlots.add(true);
				else
					timeSlots.add(false);
			}
			hops++;
			search = hops < COST_SEND_OUTPUT || timeSlots.get(time+hops);			
		}
	}
	
	public boolean portHasOutput(int whichInput){
		return !outputs[whichInput].isEmpty();
	}
	
	public void writeInput(Node node){
		this.inputs.add(node);
	}
	
	public void writeOutputToPort(int whichInput, Node node){
		this.outputs[whichInput].add(node);
	}
	
	public boolean hasInputs(){
		return !this.inputs.isEmpty();
	}
	 
	public void processInputsFromPQ(int currentTime){
		PQ.sendOutputs(tempInputBuffer, timeSlots, currentTime);
		if (timeSlots.get(currentTime)){
			inputs.add(tempInputBuffer.remove(0));
		}
	}
	
	public void writeToOutput(int currentTime){
		for (int i = 0; i < numOfPorts; i++){
			if (slotsAvailableEBO[i].get(currentTime)){
				outputs[i].add(tempOutputs[i].get(0));
			}
		}		
	}
	
	public void processInputs(int currentTime){		
		boolean slotAvailable = checkForSlot(currentTime);
		if (slotAvailable){
			if (this.hasInputs()){
				int timeToReadIntoOutput = this.reserveSlots(currentTime);
				Node input = inputs.remove(0);
				int fcount = 0;
				boolean putBack = false;
				for (Integer id:input.conn_weights.keySet()){
					Node fanout = graph.getNodeByID(id);
					if (fanout.backpointer != input.id){
						if (fanout.visited){
							int new_cost = input.curr_cost + input.conn_weights.get(id);
							if (fanout.curr_cost > new_cost){
								fanout.backpointer = input.id;
								fanout.curr_cost = new_cost;								
								reserveSlotForEBOut(fcount,currentTime,timeToReadIntoOutput);
								tempOutputs[fcount].add(fanout);
								fcount++;								
							}
						}else {
							fanout.visited = true;
							fanout.backpointer = input.id;
							fanout.curr_cost = input.curr_cost + input.conn_weights.get(id);							
							reserveSlotForEBOut(fcount,currentTime,timeToReadIntoOutput);
							tempOutputs[fcount].add(fanout);
							fcount++;
						}											
					}
					if (fcount == numOfPorts){
						putBack = true;
						break;
					}
				}
				
				if (putBack)
					inputs.add(0,input); //put back at head of queue.
				
			}else{
				this.slotsAvailable.add(true); //advance slot availability
			}
		}
	}
	
	private void reserveSlotForEBOut(int port, int currentTime, int toTime){
		while (slotsAvailableEBO[port].size() < toTime){
			slotsAvailableEBO[port].add(false);
		}
		slotsAvailableEBO[port].add(true);
	}
	
	private int reserveSlots(int time){
		for (int i = 0; i < COST_EB_PER_NODE; i++){
			slotsAvailable.add(false);
			//System.out.println("Reserved slot at time = "+time);
		}
		
		if (slotsAvailable.size() < time)
			throw new Error("Slots Availability records lagging behind currentTime. " +
					"Size="+slotsAvailable.size()+", currentTime="+time);
		
		return slotsAvailable.size();
	}
	
	private boolean checkForSlot(int time){
		return slotsAvailable.size() <= time;
	}
	
	public Node getNodeByID(int i) {
		Node node = null;
		
		for (Node n:graph.nodes){
			if (n.id == i)
				node = n;
		}
		
		assert node != null;
		return node;
	}
	
	public boolean isBusy(int currentTime){
		boolean check = tempInputBuffer.isEmpty();
		for (int i = 0; i < numOfPorts; i++){
				check = check && tempOutputs[i].isEmpty();
		}
		
		if (!hasInputs() && check && !PQ.isBusy(currentTime)){
			System.out.println("EB not busy from check...");
			return false;
		}
		
		if (foundRoute){
			System.out.println("EB not busy from found route...");
			PQ.foundRoute = true;
			return false;
		}
		
		return true;
	}
	
}
