package routing;

import java.util.ArrayList;
import java.util.TreeMap;

public class PriorityQueue {

	//private static final int  COST_EVAL = 1; //# of cycles to evaluate cost of routing to node
	//private static final int COST_POP_PQ = 1; //# of cycles to pop a value from the PQ
	//private TreeMap<Integer,ArrayList<Integer>> heap = null;
	
	//number of inputs that should be evaluated in each cycle.. i.e. number of input ports to PQ block
	private static final int numOfPorts = 4;
	
	private static final int COST_PQ_PER_CYCLE = 1; //# of cycles to move values in PQ
	private static final int COST_SEND_OUTPUT = 2; //# of cycles to send new values to expand block
	
	private ArrayList<Node>[] inputs = null; //currently assume inputs buffer to have infinite depth
	private ArrayList<Node>[] outputs = null;
	private Graph graph = null;
	private ArrayList<Boolean>[] slotsAvailable = null;
	private ExpandBlock EB = null;
	private ArrayList<Node>[] tempInputBuffer = null;
	private ArrayList<Boolean>[] timeSlotsPQ = null;
	
	public boolean foundRoute;
	
	public PriorityQueue(Graph g) {
		super();
		inputs = new ArrayList[numOfPorts];
		outputs = new ArrayList[numOfPorts];
		slotsAvailable = new ArrayList[numOfPorts];
		timeSlotsPQ = new ArrayList[numOfPorts];
		tempInputBuffer = new ArrayList[numOfPorts];
		for (int i = 0; i < numOfPorts; i++){
			inputs[i] = new ArrayList<Node>();
			outputs[i] = new ArrayList<Node>();
			tempInputBuffer[i] = new ArrayList<Node>();
			slotsAvailable[i] = new ArrayList<Boolean>();
			timeSlotsPQ[i] = new ArrayList<Boolean>();
			timeSlotsPQ[i].add(false);
			//slotsAvailable[i].add(true); // not busy at cycle 0
		}
		graph = g;		
		foundRoute = false;
	}
	
	private int roundRobin = 0; 
	public void sendOutputs(ArrayList<Node> exInputs, ArrayList<Boolean> timeSlots, int currentTime){
		
		if (!this.outputs[roundRobin].isEmpty()){
			reserveSlotForSend(timeSlots, currentTime);
			Node sendNode = this.outputs[roundRobin].remove(0);//remove first Node
			exInputs.add(sendNode);
		}else {
			timeSlots.add(false);
		}
		roundRobin++;
		roundRobin = roundRobin%numOfPorts;
		
	}
	
	public void reserveSlotForSend(ArrayList<Boolean> timeSlots, int time){
		int hops = 0;
		boolean search = hops < COST_SEND_OUTPUT || timeSlots.get(time+hops);
		while (search){
			if (timeSlots.size() == (time+hops)){
				if (hops >= COST_SEND_OUTPUT){
					timeSlots.add(true);
					//System.out.println("Added time slot here 1, "+timeSlots.size());
				}
				else{
					timeSlots.add(false);
					//System.out.println("Added time slot here 2, "+timeSlots.size());
				}
			}
			hops++;
			search = hops < COST_SEND_OUTPUT || timeSlots.get(time+hops);			
		}
	}
	
	public boolean portHasInput(int whichInput){
		return !inputs[whichInput].isEmpty();
	}
	
	public boolean portHasOutput(int whichInput){
		return !outputs[whichInput].isEmpty();
	}
	
	public void writeInput(int whichInput, Node node){
		this.inputs[whichInput].add(node);
	}
	
	public void writeOutputToPort(int whichInput, Node node){
		this.outputs[whichInput].add(node);
	}
	
	public boolean hasInputs(){
		for (int i=0; i<numOfPorts; i++)
			if (!this.inputs[i].isEmpty())
				return true;
		return false;
	}
	
	public void setEB(ExpandBlock eb){
		this.EB = eb;
	}
	
	public void processInputsFromEB(int currentTime){
		EB.sendOutputs(tempInputBuffer, timeSlotsPQ, currentTime);
		for (int i = 0; i < numOfPorts; i++){
			if (timeSlotsPQ[i].get(currentTime)){
				inputs[i].add(tempInputBuffer[i].remove(0));
			}
		}
		
	}
	
	public void processInputs(int currentTime){		
	
		for (int i = 0; i < numOfPorts; i++){
			//System.out.println("Checking slot availability at time "+currentTime+", size of slotsAvailable = "+slotsAvailable.size()+", port = "+i);
			boolean slotAvailable = this.checkForSlot(i,currentTime);
			if (slotAvailable){
				if (this.portHasInput(i)){
					Node input = inputs[i].remove(0); //take node at head of queue
					this.writeOutputToPort(i,input);
					//System.out.println("Moved node "+input.id+" from port "+i+" at time "+currentTime);
					this.reserveSlots(i,currentTime);
				}	
			}		
		}
	}
	
	public void initWriteInput(int whichInput, Node node){
		inputs[whichInput].add(node);
	}
	
	private void reserveSlots(int port, int time){
		for (int i = 0; i < COST_PQ_PER_CYCLE; i++){
			slotsAvailable[port].add(false);
			//System.out.println("Reserved slot at time = "+time);
		}
		
		if (slotsAvailable[port].size() < time)
			throw new Error("Slots Availability records lagging behind currentTime. " +
					"Size="+slotsAvailable[port].size()+", currentTime="+time);
		
	}
	
	private boolean checkForSlot(int port, int time){
		return slotsAvailable[port].size() <= time;
	}
	
	public Node getNodeByID(int i) {
		Node node = null;
		
		for (Node n:graph.nodes){
			if (n.id == i)
				node = n;
		}
		
		assert node != null;
		return node;
	}
	
	public boolean isBusy(int currentTime){
		boolean check = true;
		for (int i = 0; i < numOfPorts; i++){
				check = check && tempInputBuffer[i].isEmpty();
				check = check && outputs[i].isEmpty();
		}
		
		if (!hasInputs() && check){
			System.out.println("PQ not busy from check...");
			return false;
		}
		
		if (foundRoute){
			System.out.println("PQ not busy from found route...");
			return false;
		}
		
		return true;
	}
	
}
